<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package buchhaltungsbutler
 */

?>

<footer id="site-footer" class="site-footer">
	<div class="wrapper">
		<div class="flex-grid mb-4">
			<div class="col site-footer__widget text-center">
				<?php the_field( 'footer_widget_1', 'option' ); ?>
			</div>
			<div class="col site-footer__widget text-center">
				<?php the_field( 'footer_widget_2', 'option' ); ?>
			</div>
			<div class="col site-footer__widget text-center">
				<?php the_field( 'footer_widget_3', 'option' ); ?>
			</div>
		</div>
		<div class="site-footer__menu mb-1 text-center">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'site-footer-menu',
					'display_location' => 'site-footer-menu',
					'menu_id'        => 'site-footer-menu',
					'menu_class'     => 'site-footer-menu'
				)
			);
			?>
		</div>
		<div class="site-footer__social">
			<ul class="site-footer__social-items">
				<?php if (have_rows('site_footer_social_links', 'option')) : ?>
					<?php while (have_rows('site_footer_social_links', 'option')) : the_row(); ?>
						<li class="site-footer__social-item">
							<a class="site-footer__social-link" href="<?php the_sub_field('social_link'); ?>"><i class="<?php the_sub_field('social_icon'); ?>"></i></a>
						</li>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found 
					?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>