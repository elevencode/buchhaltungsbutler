<?php

/**
 * Author: Mario Bulic @elevencode.net
 * Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
    Utility Scripts
\*------------------------------------*/

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

function form_remove_enqueues()
{
    // Stylized select (including user and post fields)
    wp_dequeue_script('select2');
    wp_dequeue_style('select2');

    // Date picker
    wp_dequeue_script('jquery-ui-datepicker');
    wp_dequeue_style('acf-datepicker');

    // Date and time picker
    wp_dequeue_script('acf-timepicker');
    wp_dequeue_style('acf-timepicker');

    // Color picker
    wp_dequeue_script('wp-color-picker');
    wp_dequeue_style('wp-color-picker');
}
add_action('af/form/enqueue/key=form_5ede6f46dfdae', 'form_remove_enqueues');


// Enable SVG upload through media library 
function my_custom_mime_types($mimes)
{

    // New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    $mimes['doc'] = 'application/msword';

    // Optional. Remove a mime type.
    unset($mimes['exe']);

    return $mimes;
}
add_filter('upload_mimes', 'my_custom_mime_types');


// Registering footer navigation menu
add_action('after_setup_theme', 'register_footer_navigation_menu');
function register_footer_navigation_menu()
{
    register_nav_menu('site-footer-menu', __('Site Footer Menu', 'buchhaltungsbutler'));
}

//Registering options page ACF fields
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}
