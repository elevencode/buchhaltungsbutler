<?php 

/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5() {
    register_taxonomy_for_object_type( 'category', 'html5-blank' ); // Register Taxonomies for Category
    register_taxonomy_for_object_type( 'post_tag', 'html5-blank' );
    register_post_type( 'html5-blank', // Register Custom Post Type
        array(
        'labels'       => array(
            'name'               => esc_html( 'HTML5 Blank Custom Post', 'html5blank' ), // Rename these to suit
            'singular_name'      => esc_html( 'HTML5 Blank Custom Post', 'html5blank' ),
            'add_new'            => esc_html( 'Add New', 'html5blank' ),
            'add_new_item'       => esc_html( 'Add New HTML5 Blank Custom Post', 'html5blank' ),
            'edit'               => esc_html( 'Edit', 'html5blank' ),
            'edit_item'          => esc_html( 'Edit HTML5 Blank Custom Post', 'html5blank' ),
            'new_item'           => esc_html( 'New HTML5 Blank Custom Post', 'html5blank' ),
            'view'               => esc_html( 'View HTML5 Blank Custom Post', 'html5blank' ),
            'view_item'          => esc_html( 'View HTML5 Blank Custom Post', 'html5blank' ),
            'search_items'       => esc_html( 'Search HTML5 Blank Custom Post', 'html5blank' ),
            'not_found'          => esc_html( 'No HTML5 Blank Custom Posts found', 'html5blank' ),
            'not_found_in_trash' => esc_html( 'No HTML5 Blank Custom Posts found in Trash', 'html5blank' ),
        ),
        'public'       => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive'  => true,
        'supports'     => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export'   => true, // Allows export in Tools > Export
        'taxonomies'   => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ) );
}