
<section id="hero-banner" class="hero-banner" style="background-image:url('<?php echo get_field('hero_banner_image')['url']; ?>');">
    <div class="hero-banner__widget wrapper">
        <h1 class="fw-900"><?php echo get_field('hero_banner_title'); ?></h1>
        <p><?php echo get_field('hero_banner_description'); ?></p>
    </div>
</section>