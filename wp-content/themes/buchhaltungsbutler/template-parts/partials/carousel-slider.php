<section id="carousel-slider" class="carousel-slider section-bg--light" style="padding-bottom: 120px">
    <div class="wrapper">
        <div class="carousel-slider__title">
            <h2>BuchhaltungsButler in der Presse</h2>
        </div>
        <div class="carousel-slider__wrapper">
            <?php if (have_rows('slick_slider_gallery')) : ?>
                <?php while (have_rows('slick_slider_gallery')) : the_row(); ?>
                    <div class="carousel-slider__item">
                        <?php $slider_image = get_sub_field('slider_image'); ?>
                        <?php if ($slider_image) { ?>
                            <img class="carousel-slider__image" src="<?php echo $slider_image['url']; ?>" alt="<?php echo $slider_image['alt']; ?>" />
                        <?php } ?>
                        <div class="carousel-slider__item-content">
                            <div class="carousel-slider__item-title"><h3><?php the_sub_field('slider_title'); ?></h3></div>
                            <div class="carousel-slider__item-excerpt"><?php the_sub_field('slider_excerpt'); ?></div>
                        </div>
                        <a href="<?php the_sub_field('slider_link'); ?>" class="carousel-slider__item-link" rel="noopener" target="_blank"></a>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found 
                ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('.carousel-slider__wrapper').slick({
                dots: false,
                arrows: true,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                variableWidth: true,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            arrows: false,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            arrows: false,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
    })(jQuery);
</script>