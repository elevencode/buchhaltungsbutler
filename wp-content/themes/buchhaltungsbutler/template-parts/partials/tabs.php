<section id="tabs-section" class="tabs-section section-padding">
    <div class="wrapper">
        <h2 class="mb-4 text-center"><?php _e('Informationen zum Unternehmen '); ?></h2>
        <div class="flex-grid tabs-nav">
            <?php if (have_rows('tab_repeater')) : ?>
                <?php while (have_rows('tab_repeater')) : the_row(); ?>
                    <a class="col tab-link" href="#tab-<?php echo get_row_index(); ?>">
                        <span class="tab-link__image" >
                            <?php $tab_image = get_sub_field('tab_image'); ?>
                            <?php if ($tab_image) { ?>
                                <img src="<?php echo $tab_image['url']; ?>" alt="<?php echo $tab_image['alt']; ?>" />
                            <?php } ?>
                        </span>
                        <h3 class="tab-link__title text-center pb-0 pt-2"><?php the_sub_field('tab_title'); ?></h3>
                    </a>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found 
                ?>
            <?php endif; ?>
        </div>
        <div class="tab-content">
            <?php if (have_rows('tab_repeater')) : ?>
                <?php while (have_rows('tab_repeater')) : the_row(); ?>
                    <div id="tab-<?php echo get_row_index(); ?>" class="tab-content__item">
                        <?php the_sub_field('tab_description'); ?>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found 
                ?>
            <?php endif; ?>
        </div>
        <a class="btn btn--primary float-right"><?php _e('Uber uns'); ?></a> 
    </div>
</section>
<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            // Show the first tab by default
            $('.tab-content .tab-content__item').hide();
            $('.tab-content div:first').show();
            $('.tabs-nav .tab-link:first-child').addClass('tab-active');

            // Change tab class and display content
            $('.tabs-nav a').on('click', function(event) {
                event.preventDefault();
                $('.tabs-nav a').removeClass('tab-active');
                $(this).addClass('tab-active');
                $('.tab-content .tab-content__item').hide();
                $($(this).attr('href')).show();
            });
        });
    })(jQuery);
</script>