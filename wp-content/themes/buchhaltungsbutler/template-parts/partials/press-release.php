<section id="press-release" class="press-release section-bg--light section-padding">
    <div class="wrapper">
        <h2 class="text-center"><?php _e('Pressemitteilungen'); ?></h2>
        <div class="press-release__items">

            <?php
            // Example argument that defines three posts per page. 
            $args = array(
                'post_type' => 'press_release',
                'posts_per_page' => 3
            );

            // Variable to call WP_Query. 
            $the_query = new WP_Query($args);

            if ($the_query->have_posts()) :
                // Start the Loop 
                while ($the_query->have_posts()) : $the_query->the_post(); ?>

                    <div class="press-release__item">
                        <span class="press-release__item-date"><?php echo get_the_date('d.m.Y'); ?></span>
                        <h3 class="press-release__item-title"><?php the_title(); ?></h3>
                        <?php echo the_field('press_release_description'); ?>
                        <a href="<?php echo the_field('press_release_link'); ?>" class="btn btn--dark press_release__item-link">Als PDF ansehen</a>

                    </div>

            <?php
                // End the Loop
                endwhile;
            else :
                // If no posts match this query, output this text. 
                _e('Sorry, no posts matched your criteria.', 'textdomain');
            endif;

            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>