<section id="contact-form" class="contact-form section-padding">
    <div class="wrapper">
        <div class="flex-grid">
            <div class="col">
                <form>
                    <h2 class="contact-form__title"><?php echo _e('Nehmen Sie Kontakt mit uns auf'); ?></h2>
                    <p><?php echo _e('Für weitere Informationen oder Interviewanfragen wenden Sie sich bitte an Natalie'); ?> </p>
                    <?php if ( have_rows( 'contact_form' ) ) :?>
	                    <?php while ( have_rows( 'contact_form' ) ) : the_row(); ?>
                            <span class="has-float-label">
                                <input class="form-control mb-1" id="contact-form__input-field-1" type="text" placeholder="<?php the_sub_field( 'input_field_name' ); ?>" />
                                <label for="contact-form__input-field-1"><?php the_sub_field( 'input_field_name' ); ?></label>
                            </span>
                            <span class="has-float-label">
                                <input class="form-control mb-1" id="contact-form__input-field-2" type="text" placeholder="<?php the_sub_field( 'input_field_office' ); ?>" />
                                <label for="contact-form__input-field-2"><?php the_sub_field( 'input_field_office'); ?></label>
                            </span>
                            <span class="has-float-label">
                                <input class="form-control mb-1" id="contact-form__input-field-3" type="text" placeholder="<?php the_sub_field( 'input_field_email' ); ?>" />
                                <label for="contact-form__input-field-3"><?php the_sub_field( 'input_field_email' ); ?></label>
                            </span>
                            <span class="has-float-label">
                                <input class="form-control mb-1" id="contact-form__input-field-4" type="text" placeholder="<?php the_sub_field( 'input_field_callback_number' ); ?>" />
                                <label for="contact-form__input-field-4"><?php the_sub_field( 'input_field_callback_number' ); ?></label>
                            </span>
                            <span class="has-float-label">
                                <textarea class="form-control mb-1" id="contact-form__input-field-5" type="text" placeholder="<?php the_sub_field( 'textarea_message' ); ?>" ></textarea>
                                <label for="contact-form__input-field-5"><?php the_sub_field( 'textarea_message' ); ?></label>
                            </span>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>

                    <button class="btn btn--primary btn--large" type="submit">Absenden</button>
                </form>
            </div>
            <div class="col">
                <div class="text-center">
                    <h2 class="contact-form__title"><?php echo _e('Ich Anschperpartner'); ?></h2>

                    <div>
                        <?php if (have_rows('contact_form_sales_group')) : ?>
                            <?php while (have_rows('contact_form_sales_group')) : the_row(); ?>
                                <?php $contact_form_sales_image = get_sub_field('contact_form_sales_image'); ?>
                                <?php if ($contact_form_sales_image) { ?>
                                    <img class="contact-form__sales-group-image" src="<?php echo $contact_form_sales_image['url']; ?>" alt="<?php echo $contact_form_sales_image['alt']; ?>" />
                                <?php } ?>
                                <h3 class="contact-form__sales-group-title"><?php the_sub_field('contact_form_sales_name'); ?></h3>
                                <hr class="contact-form__sales-group-divider">
                                <span class="contact-form__sales-group-role d-block mb-h"><?php the_sub_field('contact_form_sales_role'); ?> </span>
                                <span class="contact-form__sales-group-phone d-block">
                                    <i class="icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/telephone.svg" />
                                    </i>
                                    <span class="d-inline-block"><?php the_sub_field('contact_form_phone'); ?></span>
                                </span>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>