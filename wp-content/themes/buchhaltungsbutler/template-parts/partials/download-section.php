<section id="download-section" class="download-section section-bg--primary section-padding">
    <div class="wrapper text-center">
        <h2 class="mb-1"><?php _e('Fotos & Grafiken'); ?></h2>
        <div><img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/cloud-download.svg"></div>
        <div>
            <p class="mb-3" style="font-size:24px;"><?php _e('Sie möchten einen Artikel, Blog oder Social Media Post über BuchhaltungsButler schreiben? Laden Sie hier unsere Fotos und Logos herunter.'); ?></p>
        </div>
        <a href="" class="btn btn--dark btn--large"><?php _e('Ressourcen herunterladen'); ?></a>
    </div>
</section>