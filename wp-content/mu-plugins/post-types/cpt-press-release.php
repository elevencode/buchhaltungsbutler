<?php

/**
 * Registering custom post type
 * Press release
 * 
 * 
 * @package buchhaltungsbutler
 */


function create_pressrelease_cpt() {

	$labels = array(
		'name' => _x( 'Press Releases', 'Post Type General Name', 'buchhaltungsbutler' ),
		'singular_name' => _x( 'Press Release', 'Post Type Singular Name', 'buchhaltungsbutler' ),
		'menu_name' => _x( 'Press Releases', 'Admin Menu text', 'buchhaltungsbutler' ),
		'name_admin_bar' => _x( 'Press Release', 'Add New on Toolbar', 'buchhaltungsbutler' ),
		'archives' => __( 'Press Release Archives', 'buchhaltungsbutler' ),
		'attributes' => __( 'Press Release Attributes', 'buchhaltungsbutler' ),
		'parent_item_colon' => __( 'Parent Press Release:', 'buchhaltungsbutler' ),
		'all_items' => __( 'All Press Releases', 'buchhaltungsbutler' ),
		'add_new_item' => __( 'Add New Press Release', 'buchhaltungsbutler' ),
		'add_new' => __( 'Add New', 'buchhaltungsbutler' ),
		'new_item' => __( 'New Press Release', 'buchhaltungsbutler' ),
		'edit_item' => __( 'Edit Press Release', 'buchhaltungsbutler' ),
		'update_item' => __( 'Update Press Release', 'buchhaltungsbutler' ),
		'view_item' => __( 'View Press Release', 'buchhaltungsbutler' ),
		'view_items' => __( 'View Press Releases', 'buchhaltungsbutler' ),
		'search_items' => __( 'Search Press Release', 'buchhaltungsbutler' ),
		'not_found' => __( 'Not found', 'buchhaltungsbutler' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'buchhaltungsbutler' ),
		'featured_image' => __( 'Featured Image', 'buchhaltungsbutler' ),
		'set_featured_image' => __( 'Set featured image', 'buchhaltungsbutler' ),
		'remove_featured_image' => __( 'Remove featured image', 'buchhaltungsbutler' ),
		'use_featured_image' => __( 'Use as featured image', 'buchhaltungsbutler' ),
		'insert_into_item' => __( 'Insert into Press Release', 'buchhaltungsbutler' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Press Release', 'buchhaltungsbutler' ),
		'items_list' => __( 'Press Releases list', 'buchhaltungsbutler' ),
		'items_list_navigation' => __( 'Press Releases list navigation', 'buchhaltungsbutler' ),
		'filter_items_list' => __( 'Filter Press Releases list', 'buchhaltungsbutler' ),
	);
	$args = array(
		'label' => __( 'Press Release', 'buchhaltungsbutler' ),
		'description' => __( 'Press Release items displayed on newsroom page', 'buchhaltungsbutler' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-testimonial',
		'supports' => array('title'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'press_release', $args );

}
add_action( 'init', 'create_pressrelease_cpt', 0 );