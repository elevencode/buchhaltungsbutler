<?php 

if (!defined('ABSPATH')) exit();

/* MySQL database table prefix. */
$table_prefix = 'wp_';

/* Authentication Unique Keys and Salts. */
/* https://api.wordpress.org/secret-key/1.1/salt/ */
define('AUTH_KEY',         '=A~qPB3]&Vkj*LAw2Cjnt=D`b+e^Gf5NM.j=b/3>&)=d2nm1^6W|a=@o/(6tm1x|');
define('SECURE_AUTH_KEY',  'KKHrp)$&>xx<#8~YCzlzw1%x|,ILIMWDsSZw7WJWYxV^mk:M;n9+clo{6g`-y~^N');
define('LOGGED_IN_KEY',    'W/3rO4),4sUdG*JqwQB|ir*d+p+xcK=h5;8C(8:7}h.t+t2A$Wqzfhbl>q_}.zLI');
define('NONCE_KEY',        '`X#}7u7|LuQP-O]8rsk6=W?~>}F-sW*}TIO!GxN_gR/Xf%u-n[9I~fbur-*6(j-R');
define('AUTH_SALT',        'r:+@/QB!K9Fi:diTMT-9xsH<r(Zvq#GE4rAS_3{~#oGkvTe8n)dj)V0`*uU93Xl(');
define('SECURE_AUTH_SALT', '9`Pm&| OE-bKSg=N~N(*bG5/cWro1e8RPAfe`[WrrH5lTZ>hK+d|Y Xyc2&Cj7n?');
define('LOGGED_IN_SALT',   'la&0-3+PJY(bQxU(OI9 z9=Zw|xZjm&tSgx$S0dFhkm!,z-Nzb7bFs<g79k1F^P`');
define('NONCE_SALT',       'q(+.:Mupv^*+Y-+Rcx}PRz>BMQE+`1`GVebvB.qRjM!X^7FgAW+r^2+FH8$J!|#U');

/* BASIC SETTINGS */

/* for Contact-Form-7 */
define('WPCF7_AUTOP', false);

/* Specify maximum number of Revisions. */
define( 'WP_POST_REVISIONS', '3' );

/* Media Trash. */
define( 'MEDIA_TRASH', false );

/* Updates */
define( 'WP_AUTO_UPDATE_CORE', false );
define( 'DISALLOW_FILE_MODS', false );
define( 'DISALLOW_FILE_EDIT', false );

/* Autosave interval */
define( 'AUTOSAVE_INTERVAL', 300 ); // Seconds

/* Debug mode */
@ini_set( 'log_errors', 'On' );
@ini_set( 'display_errors', 'Off' );
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

define('ALLOW_UNFILTERED_UPLOADS', true);

/* Multisite. */
// define( 'WP_ALLOW_MULTISITE', false );

/* Language Settings */
// define( 'WPLANG', 'de_DE' );
// define( 'WP_LANG_DIR', dirname(__FILE__) . 'wordpress/languages' );

/* ADVANCED SETTINGS */

/* Compression */
// define( 'COMPRESS_CSS',        true );
// define( 'COMPRESS_SCRIPTS',    true );
// define( 'CONCATENATE_SCRIPTS', true );
// define( 'ENFORCE_GZIP',        true );

/* SSL */
// define( 'FORCE_SSL_LOGIN', true );
// define( 'FORCE_SSL_ADMIN', true );

/* CRON */
// define( 'DISABLE_WP_CRON', false );
// define( 'ALTERNATE_WP_CRON', false );

/* Cookie domain */
// define( 'COOKIE_DOMAIN', 'www.example.com' );

/* Max Memory Limit */
// define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/* Cache */
// define( 'WP_CACHE', true );

/* Cleanup Image Edits */
// define( 'IMAGE_EDIT_OVERWRITE', true );

/* Override of default file permissions */
// define( 'FS_CHMOD_DIR', ( 0755 & ~ umask() ) );
// define( 'FS_CHMOD_FILE', ( 0644 & ~ umask() ) );

/* WordPress Upgrade Constants */

// Host running with a special installation setup involving symlinks. 
// You may need to define the path-related constants 
// (FTP_BASE, FTP_CONTENT_DIR, and FTP_PLUGIN_DIR). Often defining simply the base will be enough.

// Certain PHP installations shipped with a PHP FTP extension which is 
// incompatible with certain FTP servers. Under these rare situations, you may need to define FS_METHOD to “ftpsockets”.

// define( 'FS_METHOD', 'ftpext' );
// define( 'FTP_BASE', '/path/to/wordpress/' );
// define( 'FTP_CONTENT_DIR', '/path/to/wordpress/wp-content/' );
// define( 'FTP_PLUGIN_DIR ', '/path/to/wordpress/wp-content/plugins/' );
// define( 'FTP_PUBKEY', '/home/username/.ssh/id_rsa.pub' );
// define( 'FTP_PRIKEY', '/home/username/.ssh/id_rsa' );
// define( 'FTP_USER', 'username' );
// define( 'FTP_PASS', 'password' );
// define( 'FTP_HOST', 'ftp.example.org' );
// define( 'FTP_SSL', false );

/* Block External URL Requests */

// define( 'WP_HTTP_BLOCK_EXTERNAL', true );
// define( 'WP_ACCESSIBLE_HOSTS', 'api.wordpress.org,*.github.com' );
